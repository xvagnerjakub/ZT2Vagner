<?php

// basis oop // 
class User
{

    // private atribut //
    private $aPrivate;
    private $bPrivate;

    // protected atribut //
    protected $cProtected;
    protected $cProtected;
    protected $dProtected;
    protected $eProtected;

    // public atribut //
    public $firstP = "string";
    public $secondP = true;
    public $thirdP = 'aray';
    public $fourthP = false;
    public $fifthP = "<b>test2</b>";

    // task3 //
    public function __construct() {
    }

    // task6 //

    // private //
    public function setAPrivJakubV($aPrivate) {
        return $aPrivate;
    }

    public function setBPrivJakubV($bPrivate) {
        return $bPrivate;
    }

    //public
    public function getFPJakubV() {
        return $this->firstP;
    }

    public function getSPJakubV() {
        return $this->secondP;
    }

    public function getTPJakubV() {
        return $this->thirdP;
    }

    public function getFoPJakubV() {
        return $this->fourthP;
    }

    public function getFiPJakubV() {
        return $this->fifthP;
    }
}

$User = new User;
// set Private
echo $User->setAPrivJakubV(3.5);
echo "<br>";
echo $User->setBPrivJakubV("test2");
echo "<br>";

// get Public
echo $User->getFPJakubV();
echo "<br>";
echo $User->getSPJakubV();
echo "<br>";
echo $User->getTPJakubV();
echo "<br>";
echo $User->getFoPJakubV();
echo "<br>";
echo $User->getFiPJakubV();
echo "<br>";

var_dump($User);

    ?>