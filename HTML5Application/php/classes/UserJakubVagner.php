<?php
declare(strict_types=1);
namespace VagnerJakubUser;

/**
 * @author Jakub Vagner
 *
 */

class UserJakubVagner {
    // atributy
    private $vekPrivateJakubVagner;                  //věk
    private $znameniPrivateJakubVagner;              //znamení
    protected $vekProtectedJakubVagner;              //věk
    protected $pohlaviProtectedJakubVagner;          //pohlaví
    protected $vyskaProtectedJakubVagner;            //výška
    public $znameniPublicJakubVagner = "Panna";
    public $vyskaPublicJakubVagner = 190;
    public $vekPublicJakubVagner = 18;
    public $pohlaviPublicJakubVagner = "muz";
    public $mesicPublicJakubVagner = "zari";



    // konstruktor
    function __construct(){
    }

    /**
     * @param int $vekPrivateJakubVagner
     */
    // soukroma metoda
    function setVekPrivateJakubVagner(int $vekPrivateJakubVagner):int
    {
        return $vekPrivateJakubVagner;
        /**
         *vypíše integer, který je nadefinován (18)
         */
    }
    /**
     * @param string $znameniPrivateJakubVagner
     */


    function setZnameniPrivateJakubVagner(string $znameniPrivateJakubVagner):string
    {
        return $znameniPrivateJakubVagner;
        /**
         *vypíše nadefinovaný string (Zari)
         */
    }



    // verejna metoda
    function getZnameniPublicJakubVagner():string
    {
        return $this->znameniPublicJakubVagner;

        /**
         *vypíše nadefinovaný string (Zari)
         */
    }
    function getVyskaPublicJakubVagner():int
    {
        return $this->vyskaPublicJakubVagner;
        /**
         *vypíše integer, který je nadefinován (190)
         */
    }
    function getVekPublicJakubVagner():int
    {
        return $this->vekPublicJakubVagner;
        /**
         *vypíše integer, který je nadefinován (18)
         */
    }
    function getPohlaviPublicJakubVagner():string
    {
        return $this->pohlaviPublicJakubVagner;
        /**
         * tato je víceřádkový komentář
         * funkce slouží k vypsání informace z předešlé atributy
         */
    }
    function getMesicPublicJakubVagner():string
    {
        return $this->mesicPublicJakubVagner;
        /**
         *vypíše string
         */
    }
}
// instancovani

?>